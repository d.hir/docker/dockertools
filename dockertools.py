#!/usr/bin/env python

VERSION = '1.1.0'

import argparse
import sys
import os
import pathlib
import logging
import enum
import shutil
import json
import docker
import requests
import dataclasses
import collections


### Settings

DEFAULT_INSTALL_DIR = pathlib.Path.home() / '.scripts' / 'path'
DEFAULT_INSTALL_NAME = 'dt'

DEFAULT_DOCKER_NAMESPACE = 'nk314'
DTCONFIG_NAME = 'dt.json'
DTCONFIG_PATH = pathlib.Path.cwd() / DTCONFIG_NAME

DEFAULT_BUILD_VERSION_TAG = 'latest'

SPECIAL_VERSION_TAGS = ('latest',)


### Logging setup

LOGLEVEL = logging.DEBUG
logger = logging.getLogger('Dockertools')
logger.setLevel(LOGLEVEL)
handler = logging.StreamHandler(sys.stderr)
handler.setLevel(LOGLEVEL)
formatter = logging.Formatter(fmt='[%(name)s] %(levelname)s: %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)


class Sublogger(logging.Logger):
    def __init__(self, logger: logging.Logger, name: str):
        super().__init__(name='.'.join([logger.name, name]), level=logger.level)
        for handler in logger.handlers:
            self.addHandler(handler)


### General utilities

class DockertoolsException(Exception):
    def __init__(self, message: str = 'Exception raised in Dockertools'):
        super().__init__(message)


class DockertoolsConfigNotFound(DockertoolsException):
    def __init__(self, message: str = f'Dockertools configuration file ({DTCONFIG_PATH}) not found'):
        super().__init__(message)


def str2bool(s: str, default: bool = True) -> bool:
    if s.lower() in ('y', 'yes'):
        return True
    elif s.lower() in ('n', 'no'):
        return False
    elif s.strip() == '':
        return default
    else:
        raise DockertoolsException('Invalid input. Expected any of "y", "yes", "n", "no"')


def no_config_file(sublogger: logging.Logger, activity: str) -> None:
    sublogger.error(f'For {activity}, a valid Dockertools configuration file ("{DTCONFIG_PATH}") must exist.')
    sublogger.error('Ensure that it exists and create it with')
    exename = pathlib.Path(sys.argv[0]).name
    sublogger.error(f'\t$ {exename} config create')
    sublogger.error('if necessary.')


### Docker utilities

try:
    client = docker.DockerClient()
except docker.errors.DockerException as e: # type: ignore
    client = None


def ensure_docker_client() -> None:
    if client is None:
        raise DockertoolsException('Cannot connect to Docker engine. Ensure it is started and available on the default address and port.')


def get_remote_tags_info(image_name: str) -> list[dict]:
    config = config_read()

    image_name = '/'.join([config.namespace, image_name])

    url = f'https://hub.docker.com/v2/repositories/{image_name}/tags/'
    
    try:
        response = requests.get(url)
        response.raise_for_status()
    except requests.exceptions.RequestException as e:
        raise DockertoolsException(f'Retrieving latest version of image "{image_name}" failed:\n{e}')

    data = response.json()
    if 'results' in data:
        tags = data['results']
        return tags
    else:
        logger.warning(f'No tags found for image "{image_name}"')
        return []


def get_remote_tags(image_name: str) -> list[str]:
    tags = get_remote_tags_info(image_name)
    return [tag['name'] for tag in tags]


def get_latest_remote_tag(image_name: str) -> str:
    tags = get_remote_tags_info(image_name)
    filtered_tags = [t for t in tags if t['name'] not in SPECIAL_VERSION_TAGS]
    latest_tag = max(filtered_tags, key=lambda x: x['last_updated'])
    return latest_tag['name']


def get_local_tags(image_name: str) -> list[str]:
    ensure_docker_client()

    image = client.images.get(image_name)

    return [tag.split(':')[-1] for tag in image.tags] # type: ignore


def attach_tag(existing_tag: str, new_tag: str, sublogger: logging.Logger | None = None) -> None:
    ensure_docker_client()

    image = client.images.get(existing_tag)

    if sublogger is not None:
        sublogger.info(f'Tag:\t{existing_tag}\t->\t{new_tag}')

    ret = image.tag(*new_tag.split(':'))
    if ret is False:
        raise DockertoolsException(f'Attaching tag {new_tag} to image with tag {existing_tag} failed')


def push_tag(tag: str) -> None:
    ensure_docker_client()

    client.images.push(*tag.split(':'))

### Placeholders

def _placeholder_ver(image_name: str) -> list[str]:
    tags = get_local_tags(image_name)

    filtered_tags = [tag for tag in tags if tag not in SPECIAL_VERSION_TAGS]
    
    return [max(filtered_tags)]


def _placeholder_all(image_name: str) -> list[str]:
    return get_local_tags(image_name)


def _placeholder_new(image_name: str) -> list[str]:
    remote_tags = get_remote_tags(image_name)
    remote_set = set(remote_tags)
    local_tags = get_local_tags(image_name)
    local_set = set(local_tags)

    local_surplus = local_set - remote_set
    
    return list(local_surplus)


def CLI_PLACEHOLDER(s: str) -> str:
    return f'+{s}+'


class Placeholder:
    def __init__(self, description: str, expand: collections.abc.Callable[[str], str]): # type: ignore
        self.description = description
        self.expand = expand


PLACEHOLDERS = {
    CLI_PLACEHOLDER('ver'): Placeholder('Placeholder for the latest locally built version', _placeholder_ver),
    CLI_PLACEHOLDER('all'): Placeholder('Placeholder for all locally built versions', _placeholder_all),
    CLI_PLACEHOLDER('new'): Placeholder('Placeholder for all locally built versions that are not present in the remote repository', _placeholder_new)
}


### Installation

class InstallationMethod(str, enum.Enum):
    copy = 'copy'
    symlink = 'symlink'
    hardlink = 'hardlink'
    default = hardlink

    def execute(self, source: pathlib.Path, target: pathlib.Path):
        if self.value == 'copy':
            shutil.copy2(source, target)
        elif self.value == 'symlink':
            os.symlink(source, target)
        elif self.value == 'hardlink':
            os.link(source, target)


def _install(args: argparse.Namespace) -> None:
    sublogger = Sublogger(logger, 'install')
    sublogger.info('Installing Dockertools')

    script = pathlib.Path(os.path.abspath(__file__))
    sublogger.debug(f'Dockertools source script: "{script}"')

    sublogger.info(f'Ensuring target directory "{args.dir}" exists')
    os.makedirs(args.dir, exist_ok=True)

    sublogger.info('Checking if there is already an installed program at the target path')
    target: pathlib.Path = args.dir / args.name
    if target.exists():
        if args.update:
            overwrite = True
        else:
            sublogger.warning(f'Target already exists: "{target}". Overwrite? [Y/n]')
            overwrite = str2bool(input())
        if overwrite:
            if not target.is_file():
                raise DockertoolsException('Target is a directory. For safety reasons, please manually remove the directory and restart installation.')
            target.unlink()
            sublogger.debug(f'Removed file "{target}"')

    sublogger.info(f'Creating installation target "{args.name}"')
    args.method.execute(script, target)


def attach_install_parser(subparsers) -> None:
    install = subparsers.add_parser('install')
    install.set_defaults(handler=_install)
    install.add_argument('--dir', default=DEFAULT_INSTALL_DIR, type=pathlib.Path,
                         help=f'Directory under which to install Dockertools (must be on PATH to be accessible from everywhere) [default: "{DEFAULT_INSTALL_DIR}"]')
    install.add_argument('--name', default=DEFAULT_INSTALL_NAME,
                         help=f'Name of the installation target, i.e. command for executing Dockertools [default: "{DEFAULT_INSTALL_NAME}"]')
    install.add_argument('-m', '--method', choices=[m.value for m in InstallationMethod], default=InstallationMethod.default, type=InstallationMethod,
                         help=f'Installation method (the specified object is placed in the installation dir) [default: "{InstallationMethod.default.value}"]')
    install.add_argument('-u', '--update', action='store_true',
                         help=f'Overwrite any existing file without asking first')


### Config


class VersioningScheme(dict, enum.Enum):
    semantic = {
        'initial_version': '1.0.0'
    }


@dataclasses.dataclass
class Config:
    repository: str
    namespace: str
    versioning_scheme: str
    version: str


def _config_create(args: argparse.Namespace) -> None:
    sublogger = Sublogger(Sublogger(logger, 'config'), 'create')

    if DTCONFIG_PATH.exists():
        sublogger.warning(f'Configuration file "{DTCONFIG_PATH}" already exists. Recreate and overwrite? [Y/n]')
        recreate = str2bool(input(), True)
        if not recreate:
            sublogger.warning('Aborting configuration file creation')
            return

    namespace = args.namespace

    if args.repository is None:
        if DTCONFIG_PATH.exists():
            old_config = config_read()
            default_repository = old_config.repository
        else:
            default_repository = pathlib.Path.cwd().stem
        sublogger.info(f'Name of the image/repository: [{default_repository}]')
        repository = input()
        if repository.strip() == '':
            repository = default_repository
    else:
        repository = args.repository

    versioning_scheme = args.versioning_scheme.name

    if args.version is None:
        version = args.versioning_scheme['initial_version']
    else:
        version = args.version

    config = Config(
        repository = repository,
        namespace = namespace,
        versioning_scheme = versioning_scheme,
        version = version
    )

    config_write(config, sublogger)


def config_write(config: Config, sublogger: logging.Logger | None = None) -> None:
    config_dict = dataclasses.asdict(config)

    if sublogger is not None:
        sublogger.debug(f'Creating Dockertools config at "{DTCONFIG_PATH}" with the following settings: ')
        for line in json.dumps(config_dict, indent=4).split('\n'):
            sublogger.debug(line)

    with open(DTCONFIG_PATH, 'w+') as f:
        json.dump(config_dict, f, indent=4)


def config_read() -> Config:
    if not DTCONFIG_PATH.exists():
        raise DockertoolsConfigNotFound
    with open(DTCONFIG_PATH, 'r') as f:
        config_dict = json.load(f)
    return Config(**config_dict)


def _config(args: argparse.Namespace) -> None:
    pass


def attach_config_parser(subparsers) -> None:
    config: argparse.ArgumentParser = subparsers.add_parser('config')
    config.set_defaults(handler=_config)
    subconfig_parsers = config.add_subparsers(dest='config_command')

    config_create: argparse.ArgumentParser = subconfig_parsers.add_parser('create', formatter_class=argparse.RawTextHelpFormatter)
    config_create.set_defaults(handler=_config_create)
    config_create.add_argument('-r', '--repository',
                               help='Name of the repository (used in the registry)')
    config_create.add_argument('-s', '--namespace', default=DEFAULT_DOCKER_NAMESPACE,
                               help=f'Namespace under which the Docker image should be pushed to the registry [default: "{DEFAULT_DOCKER_NAMESPACE}"]')
    config_create.add_argument('--versioning-scheme', choices=[s.name for s in VersioningScheme], default=VersioningScheme.semantic, type=VersioningScheme,
                               help='Versioning scheme to use for updating the version [default: "semantic"]')
    config_create.add_argument('-v', '--version',
                               help='Current (initial) version of the image [default depends on versioning scheme:' + ''.join(f'\n - {s.name}: ' + s['initial_version'] for s in VersioningScheme))


### build

def _build(args: argparse.Namespace) -> None:
    sublogger = Sublogger(logger, 'build')

    if args.tag is None:
        no_config_file(sublogger, 'building')
        return

    ensure_docker_client()

    tag = args.tag
    if args.version.strip() != '' or (args.version.strio() == DEFAULT_BUILD_VERSION_TAG and ':' in args.tag):
        tag += f':{args.version}'

    sublogger.info(f'Starting build of "{tag}"')

    image, teeobj = client.images.build(path='.', rm=True, tag=tag)
    for item in teeobj:
        for key, value in item.items():
            if key == 'stream':
                text = value.strip()
                if text != '':
                    sublogger.debug(text)


def attach_build_parser(subparsers) -> None:
    try:
        config = config_read()
        default_tag = config.repository
        default_tag_str = f' [default: "{default_tag}"]'
    except DockertoolsConfigNotFound:
        default_tag = None
        default_tag_str = ''
    build: argparse.ArgumentParser = subparsers.add_parser('build')
    build.set_defaults(handler=_build)
    build.add_argument('-t', '--tag', default=default_tag,
                       help=f'Tag for the newly built image{default_tag_str}')
    build.add_argument('--version', default=DEFAULT_BUILD_VERSION_TAG,
                       help=f'Version part of the tag for the newly built image [default: "{DEFAULT_BUILD_VERSION_TAG}"]')


### Push

def _expand_placeholders(original: list[str], repository: str) -> list[str]:
    versions = []

    for version in original:
        if version in PLACEHOLDERS.keys():
            placeholder = PLACEHOLDERS[version]
            expanded = placeholder.expand(repository)
            versions.extend(expanded)
        else:
            versions.append(version)

    return versions


def _push_wrapper(error: str | None = None) -> collections.abc.Callable[[argparse.Namespace], None]: # type: ignore

    def _push(args: argparse.Namespace) -> None:
        sublogger = Sublogger(logger, 'push')
        if error is not None:
            for line in error.split('\n'):
                sublogger.error(line)
            no_config_file(sublogger, 'pushing')
            return
        
        config = config_read()

        repository = args.repository if args.repository is not None else config.repository

        versions = _expand_placeholders(args.versions, repository)

        versions = list(set(versions))
        full_versions = {f'{repository}:{version}': f'{config.namespace}/{repository}:{version}' for version in versions}

        sublogger.info('Pushing the following tags to the registry:')
        for full_version in full_versions.values():
            sublogger.info(f' - {full_version}')
        sublogger.info('Correct and continue? [Y/n]')
        ok = str2bool(input(), True)
        if not ok:
            return

        sublogger.info('Attaching tags to images')
        for local, full in full_versions.items():
            attach_tag(local, full, sublogger)

        for tag in full_versions.values():
            sublogger.info(f'Pushing tag {tag} to registry')
            push_tag(tag)

    return _push

def attach_push_parser(subparsers) -> None:
    try:
        config = config_read()
        config_name = config.repository
        config_name_str = f' [default: "{config_name}"]'
        error = None
    except (DockertoolsConfigNotFound, RuntimeError) as e:
        error = str(e)
        config_name = None
        config_name_str = ''
    push: argparse.ArgumentParser = subparsers.add_parser('push', formatter_class=argparse.RawTextHelpFormatter)
    push.set_defaults(handler=_push_wrapper(error))
    push.add_argument('-r', '--repository', default=config_name,
                      help=f'Repository name to push to{config_name_str}')
    default_versions = ['latest', CLI_PLACEHOLDER('new')]
    default_versions_str = ' '.join(default_versions)
    push.add_argument('--versions', nargs='+', default=default_versions,
                      help=f'List of versions to push [default: "{default_versions_str}"]'
                        + ''.join(f'\n - "{name}": {placeholder.description}' for name, placeholder in PLACEHOLDERS.items()))


### CLI parsing / main

def _create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description=f'Dockertools Version {VERSION}')
    parser.set_defaults(handler=_default_handler)

    subparsers = parser.add_subparsers(dest='command')
    attach_install_parser(subparsers)
    attach_config_parser(subparsers)
    attach_build_parser(subparsers)
    attach_push_parser(subparsers)

    return parser


def _parse_args(clargs: list[str]) -> argparse.Namespace:
    parser = _create_parser()
    return parser.parse_args(clargs)


def _default_handler(args: argparse.Namespace) -> None:
    print('Default handler')


def _main(clargs: list[str]) -> int:
    try:
        args = _parse_args(clargs)
        args.handler(args)
    except DockertoolsException as e:
        logger.error(str(e))
    return 0


if __name__ == '__main__':
    sys.exit(_main(sys.argv[1:]))
